﻿using System;
using System.Text.RegularExpressions;

namespace EvenNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = Console.ReadLine();

            string pattern = @"[0-9]+";

            Regex regex = new Regex(pattern);

            var numbers = regex.Matches(text);
            int maxEvenNumber = -1;
            foreach (var number in numbers)
            {
                int value = int.Parse(number.ToString());
                if (value % 2 == 0 && value > maxEvenNumber)
                {
                    maxEvenNumber = value;
                }
            }

            Console.WriteLine(maxEvenNumber);
        }
    }
}
