﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrimesToN
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            List<int> sb = new List<int>() { 1 };
            if (n >= 3)
            {
                sb.Add(2);
                sb.Add(3);
            }
            else if (n >= 2)
            {
                sb.Add(2);
            }
            for (int i = 4; i <= n; i++)
            {
                int currentValue = i;
                bool prime = false;
                for (int j = 2; j < currentValue; j++)
                {
                    if (currentValue % j == 0)
                    {
                        prime = false;
                        break;
                    }
                    else
                    {
                        prime = true;
                    }

                }

                if (prime)
                {
                    sb.Add(currentValue);
                }
            }

            Console.WriteLine(string.Join(" ", sb));
        }
    }
}
