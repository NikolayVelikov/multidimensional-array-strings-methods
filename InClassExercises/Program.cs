﻿using System;
using System.Text;

namespace InClassExercises
{
    class Program
    {
        public static object StringBulder { get; private set; }

        static void Main(string[] args)
        {
            string array = Console.ReadLine();

            StringBuilder maxSequance = new StringBuilder();
            StringBuilder currentSequance = new StringBuilder();

            char previousLetter = '\0';

            foreach (var letter in array)
            {
                if (currentSequance.Length == 0)
                {
                    currentSequance.Append(letter);
                    maxSequance = currentSequance;
                    previousLetter = letter;
                    continue;
                }

                if (previousLetter == letter)
                {
                    currentSequance.Append(letter);

                    if (currentSequance.Length > maxSequance.Length)
                    {
                        maxSequance = currentSequance;
                    }
                }
                else
                {                    
                    currentSequance = new StringBuilder();
                    currentSequance.Append(letter);
                    previousLetter = letter;
                }
            }

            Console.WriteLine(maxSequance.ToString().TrimEnd());
        }
    }
}
