﻿using System;

namespace KightMoves
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            int[,] arena = new int[n, n];

            int row = 0;
            int col = 0;
            arena[row, col] = 1;
            int value = 2;
            bool play = true;

            while (play)
            {
                bool canMove = true;
                int[] rowCol = new int[2];

                rowCol = UpLeftLong(arena, row, col);
                if (rowCol[0] != 0 || rowCol[1] != 0)
                {
                    Moving(arena, out row, out col, value, out canMove, rowCol);
                }

                rowCol = UpRightLong(arena, row, col);
                if ((rowCol[0] != 0 || rowCol[1] != 0) && canMove)
                {
                    Moving(arena, out row, out col, value, out canMove, rowCol);
                }

                rowCol = UpLeftShort(arena, row, col);
                if ((rowCol[0] != 0 || rowCol[1] != 0) && canMove)
                {
                    Moving(arena, out row, out col, value, out canMove, rowCol);
                }

                rowCol = UpRightShort(arena, row, col);
                if ((rowCol[0] != 0 || rowCol[1] != 0) && canMove)
                {
                    Moving(arena, out row, out col, value, out canMove, rowCol);
                }

                rowCol = DownLeftShort(arena, row, col);
                if ((rowCol[0] != 0 || rowCol[1] != 0) && canMove)
                {
                    Moving(arena, out row, out col, value, out canMove, rowCol);
                }

                rowCol = DownRightShort(arena, row, col);
                if ((rowCol[0] != 0 || rowCol[1] != 0) && canMove)
                {
                    Moving(arena, out row, out col, value, out canMove, rowCol);
                }

                rowCol = DownLeftLong(arena, row, col);
                if ((rowCol[0] != 0 || rowCol[1] != 0) && canMove)
                {
                    Moving(arena, out row, out col, value, out canMove, rowCol);
                }

                rowCol = DownRightLong(arena, row, col);
                if ((rowCol[0] != 0 || rowCol[1] != 0) && canMove)
                {
                    Moving(arena, out row, out col, value, out canMove, rowCol);
                }

                if (canMove)
                {
                    rowCol = TopLeftFreeSpace(arena);
                    row = rowCol[0];
                    col = rowCol[1];
                    if (row == 0 && col == 0)
                    {
                        play = false;
                        continue;
                    }
                    arena[row, col] = value;
                }

                value++;
            }

            Print(arena);
        }

        static void Moving(int[,] arena, out int row, out int col, int value, out bool canMove, int[] rowCol)
        {
            row = rowCol[0];
            col = rowCol[1];
            arena[row, col] = value;

            canMove = false;
        }
        static int[] TopLeftFreeSpace(int[,] matrix)
        {
            int[] rowCol = new int[2];
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    if (matrix[row, col] == 0)
                    {
                        rowCol[0] = row;
                        rowCol[1] = col;

                        return rowCol;
                    }
                }
            }

            return rowCol;
        } // Checking for free top left Space and if can continue
        static bool CheckingMoving(int[,] matrix, int row, int col)
        {
            if ((row >= 0 && row < matrix.GetLength(0)) && (col >= 0 && col < matrix.GetLength(1)))
            {
                return true;
            }

            return false;
        }
        static int[] UpLeftLong(int[,] matrix, int currentRow, int currentCol)
        {
            int[] newRowAndCol = new int[2];
            int row = currentRow - 2;
            int col = currentCol - 1;

            if (!CheckingMoving(matrix, row, col))
            {
                row = 0;
                col = 0;
            }
            if (matrix[row, col] != 0)
            {
                row = 0;
                col = 0;
            }

            newRowAndCol[0] = row;
            newRowAndCol[1] = col;

            return newRowAndCol;
        }
        static int[] UpRightLong(int[,] matrix, int currentRow, int currentCol)
        {
            int[] newRowAndCol = new int[2];
            int row = currentRow - 2;
            int col = currentCol + 1;

            if (!CheckingMoving(matrix, row, col))
            {
                row = 0;
                col = 0;
            }
            if (matrix[row, col] != 0)
            {
                row = 0;
                col = 0;
            }

            newRowAndCol[0] = row;
            newRowAndCol[1] = col;

            return newRowAndCol;
        }
        static int[] UpLeftShort(int[,] matrix, int currentRow, int currentCol)
        {
            int[] newRowAndCol = new int[2];
            int row = currentRow - 1;
            int col = currentCol - 2;

            if (!CheckingMoving(matrix, row, col))
            {
                row = 0;
                col = 0;
            }
            if (matrix[row, col] != 0)
            {
                row = 0;
                col = 0;
            }

            newRowAndCol[0] = row;
            newRowAndCol[1] = col;

            return newRowAndCol;
        }
        static int[] UpRightShort(int[,] matrix, int currentRow, int currentCol)
        {
            int[] newRowAndCol = new int[2];
            int row = currentRow - 1;
            int col = currentCol + 2;

            if (!CheckingMoving(matrix, row, col))
            {
                row = 0;
                col = 0;
            }
            if (matrix[row, col] != 0)
            {
                row = 0;
                col = 0;
            }

            newRowAndCol[0] = row;
            newRowAndCol[1] = col;

            return newRowAndCol;
        }
        static int[] DownLeftLong(int[,] matrix, int currentRow, int currentCol)
        {
            int[] newRowAndCol = new int[2];
            int row = currentRow + 2;
            int col = currentCol - 1;

            if (!CheckingMoving(matrix, row, col))
            {
                row = 0;
                col = 0;
            }
            if (matrix[row, col] != 0)
            {
                row = 0;
                col = 0;
            }

            newRowAndCol[0] = row;
            newRowAndCol[1] = col;

            return newRowAndCol;
        }
        static int[] DownRightLong(int[,] matrix, int currentRow, int currentCol)
        {
            int[] newRowAndCol = new int[2];
            int row = currentRow + 2;
            int col = currentCol + 1;

            if (!CheckingMoving(matrix, row, col))
            {
                row = 0;
                col = 0;
            }
            if (matrix[row, col] != 0)
            {
                row = 0;
                col = 0;
            }

            newRowAndCol[0] = row;
            newRowAndCol[1] = col;

            return newRowAndCol;
        }
        static int[] DownLeftShort(int[,] matrix, int currentRow, int currentCol)
        {
            int[] newRowAndCol = new int[2];
            int row = currentRow + 1;
            int col = currentCol - 2;

            if (!CheckingMoving(matrix, row, col))
            {
                row = 0;
                col = 0;
            }
            if (matrix[row, col] != 0)
            {
                row = 0;
                col = 0;
            }

            newRowAndCol[0] = row;
            newRowAndCol[1] = col;

            return newRowAndCol;
        }
        static int[] DownRightShort(int[,] matrix, int currentRow, int currentCol)
        {
            int[] newRowAndCol = new int[2];
            int row = currentRow + 1;
            int col = currentCol + 2;

            if (!CheckingMoving(matrix, row, col))
            {
                row = 0;
                col = 0;
            }
            if (matrix[row, col] != 0)
            {
                row = 0;
                col = 0;
            }

            newRowAndCol[0] = row;
            newRowAndCol[1] = col;

            return newRowAndCol;
        }
        static void Print(int[,] matrix)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    if (col != matrix.GetLength(1) - 1)
                    {
                        Console.Write(matrix[row,col] + " ");
                    }
                    else
                    {
                        Console.WriteLine(matrix[row,col]);
                    }
                }
            }
        }

    }
}
