﻿using System;
using System.Linq;
using System.Numerics;

namespace Navigation
{
    class Program
    {
        static void Main(string[] args)
        {
            int row = int.Parse(Console.ReadLine());
            int col = int.Parse(Console.ReadLine());
            BigInteger[,] matrix = FillingMatrix(row, col);
            int coef = Math.Max(row, col);

            row = row - 1;
            col = 0;
            BigInteger sum = matrix[row, col];
            matrix[row, col] = 0;



            int n = int.Parse(Console.ReadLine());
            int[] coeds = Console.ReadLine().Split(' ').Select(x => int.Parse(x)).ToArray();
            for (int i = 0; i < n; i++)
            {
                int code = coeds[i];
                int newRow = (int)code / coef;
                int newCol = (int)code % coef;

                MovingCol(matrix, row, ref col, newCol, ref sum);
                MovingRow(matrix, ref row, col, newRow, ref sum);
            }

            Console.WriteLine(sum);
        }
        static void MovingCol(BigInteger[,] matrix, int row, ref int col, int newCol, ref BigInteger sum)
        {
            int minCol = Math.Min(col, newCol);
            int maxCol = Math.Max(col, newCol);

            for (int i = minCol; i <= maxCol; i++)
            {
                sum += matrix[row, i];
                matrix[row, i] = 0;
            }

            col = newCol;
        }
        static void MovingRow(BigInteger[,] matirx, ref int row, int col, int newRow, ref BigInteger sum)
        {
            int minRow = Math.Min(row, newRow);
            int maxRow = Math.Max(row, newRow);

            for (int i = minRow; i <= maxRow; i++)
            {
                sum += matirx[i, col];
                matirx[i, col] = 0;
            }

            row = newRow;
        }

        static BigInteger[,] FillingMatrix(int row, int col)
        {
            BigInteger[,] matrix = new BigInteger[row, col];
            BigInteger counter = 1;

            for (int currentRow = (int)matrix.GetLength(0) - 1; currentRow >= 0; currentRow--)
            {
                for (int currentCol = 0; currentCol < (int)matrix.GetLength(1); currentCol++)
                {
                    matrix[currentRow, currentCol] = counter;
                    counter *= 2;
                }

                counter = matrix[currentRow, 0] * 2;
                if (currentRow == 0)
                {
                    break;
                }
            }

            return matrix;
        }
        static void PrintMatrix(BigInteger[,] matrix)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    Console.Write(matrix[row, col] + " ");
                }

                Console.WriteLine();
            }
        }
    }
}
