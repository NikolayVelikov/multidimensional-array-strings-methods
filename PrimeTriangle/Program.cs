﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrimeTriangle
{
    class Program
    {
        static void Main(string[] args)
        {
            int length = int.Parse(Console.ReadLine());
            int b = 0;
            int[] numbers = new int[length];
            int counter = 1;
            for (int index = 0; index <= length - 1; index++)
            {
                numbers[index] = counter++;
            }
            
            int primeCounter = 0;
            List<int> noPrime = new List<int>();
            List<int> primeNimbers = new List<int>();

            foreach (int num in numbers)
            {
                bool prime = true;
                if (num <= 3)
                {
                    primeNimbers.Add(num);
                    primeCounter++;
                    continue;
                }
                for (int i = 2; i < num; i++)
                {
                    if (num % i == 0)
                    {
                        prime = false;
                        noPrime.Add(num);
                        break;
                    }
                }

                if (prime)
                {
                    primeCounter++;
                    primeNimbers.Add(num);
                }
            }

            foreach(var num in primeNimbers)
            {
                StringBuilder valueForPrinting = new StringBuilder();
                for (int j = 1; j <= num; j++)
                {

                    if (noPrime.Contains(j))
                    {
                        valueForPrinting.Append(0);
                    }
                    else
                    {
                        valueForPrinting.Append(1);
                    }
                }

                Console.WriteLine(valueForPrinting.ToString().TrimEnd());
            }
        }
    }
}
