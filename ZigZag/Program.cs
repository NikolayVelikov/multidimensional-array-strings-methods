﻿using System;
using System.Linq;

namespace ZigZag
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] dimenssions = Console.ReadLine().Split(' ').Select(x => int.Parse(x)).ToArray();
            int row = dimenssions[0];
            int col = dimenssions[1];

            ulong sum = 0;

            bool isMatrixBigger = true; // if matrix is with bigger that row > 1 && col > 1

            int max = int.MaxValue;
            if (row == 1 || col == 1)
            {
                sum = 1;
                isMatrixBigger = false;
            }

            if (isMatrixBigger)
            {
                for (int currentRow = 0; currentRow < row; currentRow++)
                {
                    for (int currentCol = 0; currentCol < col; currentCol++)
                    {
                        if (FirstAdd(currentRow, currentCol)) // Add first time
                        {
                            Sum(currentRow, currentCol, ref sum);
                        }

                        if (SecondAdd(currentRow, currentCol, row, col)) // Add second time
                        {
                            Sum(currentRow, currentCol, ref sum);
                        }
                    }
                }
            }

            Console.WriteLine(sum);
        }
        static bool FirstAdd(int currentRow, int currentCol)
        {
            return (currentRow % 2 == 0 && currentCol % 2 == 0) || (currentRow % 2 != 0 && currentCol % 2 != 0);
        }
        static bool SecondAdd(int currentRow, int currentCol, int row, int col)
        {
            return currentRow != 0 && currentRow != row - 1 && currentCol != 0 && currentCol != col - 1 && FirstAdd(currentRow, currentCol);
        }
        static ulong Sum(int row, int col, ref ulong sum)
        {
            return sum += (ulong)(1 + 3 * row + 3 * col);
        }
    }
}
