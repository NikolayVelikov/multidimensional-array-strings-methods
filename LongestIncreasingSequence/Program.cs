﻿using System;

namespace LongestIncreasingSequence
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            int maxSequance = int.MinValue;
            int previousNumber = -1;
            int currentLength = 0;

            for (int i = 0; i < n; i++)
            {
                int number = int.Parse(Console.ReadLine());
                if (previousNumber == -1)
                {
                    previousNumber = number;
                    currentLength++;
                    continue;
                }

                if (previousNumber < number)
                {
                    currentLength++;
                    previousNumber = number;
                }
                else
                {
                    if (currentLength > maxSequance)
                    {
                        maxSequance = currentLength;
                    }

                    previousNumber = number;
                    currentLength = 1;
                }
            }

            Console.WriteLine(maxSequance);
        }
    }
}
